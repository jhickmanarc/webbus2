/*
 * Example of exporting shared functionality using Web Bus.
 * If imported directly by import() the "this" object can be changed
 * but the Closure will always be the library.
 *
 * Optionally using eval(web_bus.import_src("name-exported-as")) will
 * re-create the function within the Closure where eval() executed.
 */

(function (web_bus) {
	function shared_closure_function() {
		alert("Test shared variable: " + closure_var);	
	}
	web_bus.export("get-shared_closure_function", shared_closure_function);
})(wb);