function factory_event_send(web_bus, name, el, params) {
	this._setup = function(d) {
		console.log("_setup called on " + name);
		setup();
	};
	this._start = function(d) {
		console.log("_start called on " + name);
	}
	
	function setup() {
		el.querySelector(".button_send_all").addEventListener("click", function(e) {
			web_bus.fire_event("event_test", "Bradcast to all listeners.");
		});
		
		el.querySelector(".button_send_first").addEventListener("click", function(e) {
			web_bus.fire_event("container-1/event_test", "Bradcast first container.");
		});
		
		el.querySelector(".button_send_second").addEventListener("click", function(e) {
			web_bus.fire_event("container-2/event_test", "Bradcast second container.");
		});
	}
}
