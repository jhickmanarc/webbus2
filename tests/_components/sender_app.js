function factory_sent_to_app(web_bus, name, el, params) {
    this._setup = function(d) {
		console.log("_setup called on " + name);
		setup();
	};
	this._start = function(d) {
		console.log("_start called on " + name);
	}
	
	function setup() {
        el.querySelector(".button_send_all").addEventListener(
            "click",
            function(e) {
                web_bus.fire_event(
                    "event_test",
                    "Sent to all Apps");
            }
        );
        el.querySelector(".button_send_first").addEventListener(
            "click",
            function(e) {
                web_bus.fire_event(
                    "event_test",
                    "Sent to first App", 
                    "app1");
            }
        );
        el.querySelector(".button_send_second").addEventListener(
            "click",
            function(e) {
                web_bus.fire_event(
                    "event_test",
                    "Sent to the second App", 
                    "app2");
            }
        );
    }

    return this;
}