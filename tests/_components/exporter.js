function factory_export_text(web_bus, name, el, params) {
	this._setup = function(d) {
		console.log("_setup called on " + name);
		web_bus.export("shared_function", to_share);
	};
	this._start = function(d) {
		console.log("_start called on " + name);
		el.innerHTML = "Exporting function from: " + name;
	}
	
	this.msg = "In the Exporting script.";
	
	function to_share() {
		alert(this.msg);
	}
}
