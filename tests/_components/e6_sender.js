class Sender extends ES6action {
	_setup() {
		this.el.querySelector(".button_send_all").addEventListener("click", (e) => {
			this.web_bus.fire_event("event_test", "Broadcast to all listeners.");
		});
		this.el.querySelector(".button_send_first").addEventListener("click", (e) => {
			this.web_bus.fire_event("container-1/event_test", "Broadcast first container.");
		});
		this.el.querySelector(".button_send_second").addEventListener("click", (e) => {
			this.web_bus.fire_event("container-2/event_test", "Broadcast second container.");
		});
	};
}