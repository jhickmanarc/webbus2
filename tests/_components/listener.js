function factory_event_listen(web_bus, name, el, params) {
	this._setup = function(d) {
		console.log("_setup called on " + name);
	};
	this._start = function(d) {
		console.log("_start called on " + name);
	}
	
	this.event_test = function(s) {
		console.log("event_test on " + name +" called and passed: " + s);
		var ne = document.createElement("div");
		ne.innerHTML = s;
		el.appendChild(ne);
	}
}
