/*
 * A simple component built in VUE 2
 */
function FACTORY_vue_test(web_bus, name, el, params) {
	this._setup = function(d) {
		setup_vue();
	};
	
	this._start = function(d) {};
	
	this.event_set_vue_message = function (d) {
	   app.message = d; // Set a Reactive property on the VUE instance
	};
	
	var app = null; // Container variable for the VUE instance
	
	function setup_vue() {
		app = new Vue({
			el: el,
			data: {
				message: 'Hello from a Vue component!',
				button_clicked: 0
			},
			methods: {
			   button_was_clicked: function () {
			     this.button_clicked++;
			     web_bus.fire_event("event_test", this.button_clicked); // Access the Web Bus methods from within VUE
			   }
			}
		});
	}
}

/*
 * A pure JavaScript component to demonstrate communication to and from the VUE component
 */
function FACTORY_test_interaction(web_bus, name, el, params) {
  this._setup = function(d) {
      el_count = el.querySelector(".button_count");
      el_text = el.querySelector(".input_text");
      el.querySelector(".send_button").addEventListener("click", function () {
          web_bus.fire_event("event_set_vue_message", el_text.value);
          el_text.value = "";
      });
  };
  
  this._start = function(d) {};
  
  this.event_test = function (d) {
      el_count.innerHTML = d;
  };
  
  var el_count = null;
  var el_text = null;
}