function factory_dump_params(web_bus, name, el, params) {
	this._setup = function(d) {
		console.log("_setup called on " + name);
	};
	this._start = function(d) {
		console.log("_start called on " + name);
		dump_params();
	};
	
	function dump_params() {
		el.innerHTML = JSON.stringify(params);
	}
}
