function factory_import_test(web_bus, name, el, params) {
	this._setup = function(d) {
		console.log("_setup called on " + name);
	};
	this._start = function(d) {
		console.log("_start called on " + name);
		el.innerHTML = "Importing function to: " + name;
		/* 
		 * Note: we can pass the context of the "this" for the imported
		 * function but the Closure will always be the exporting
		 * component or library.
		 */
		imported_function = web_bus.import("shared_function", this);
		imported_function();
		
		/*
		 * This function was defined in the Library module and
		 * re-created in the current Closure by the eval()
		 * statement below. Of course this forces a recompile
		 * of the function and may impact start up times.
		 */
		shared_closure_function();
	}
	
	var imported_function = null;
	
	this.msg = "Hello from the importing script.";
	
	var closure_var = "This is a variable in the current Closure.";
	
	/*
	 * This line re-creates the function from the library in the current closure.
	 * This allows re-use of code defined in Web Bus shared function tracking in
	 * the current Closure.
	 */
	eval(web_bus.import_src("get-shared_closure_function"));
}
