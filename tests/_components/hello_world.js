function factory_hello_world(web_bus, name, el, params) {
	this._setup = function(d) {
		console.log("_setup called on " + name);
	};
	this._start = function(d) {
		console.log("_start called on " + name);
		el.innerHTML = "Hello World named: " + name;
	}
}
