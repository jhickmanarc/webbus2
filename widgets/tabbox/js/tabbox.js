function webbus__tabbox_factory(web_bus, name, el, params) {
	var tabs = [];
	var views = [];
	var names = {};
	
	var CTRL_CODE = 17;
	var ALT_CODE = 18;
		
	var ctrl_codes = {};
	var alt_codes = {};
	var combo_codes = {};
	var ctrl = false;
	var alt = false;
	
	var event_name = 'event_active_tabbox_view_changed';
	
	this._setup = function(d) {
		setup();
	};
	
	this._start = function(d) {};
	
	this.event_set_tabbox_view = function(tn) {
		// If the selected name in the names switch to that tab.
		if (names.hasOwnProperty(tn)) {
			set_view_by_name(tn);
			return true;
		}
		return false;
	};
	
	this.event_shortcut_ctrl = function(key) {
		if (ctrl_codes.hasOwnProperty(key)) {
			set_view_by_name(ctrl_codes[key]);
		}
	};
	
	this.event_shortcut_alt = function(key) {
		if (alt_codes.hasOwnProperty(key)) {
			set_view_by_name(alt_codes[key]);
		}
	};
	
	this.event_shortcut_ctrlalt = function(key) {
		if (combo_codes.hasOwnProperty(key)) {
			set_view_by_name(combo_codes[key]);
		}
	};
	
	function set_view(idx) {
		for (var i = 0; i < tabs.length; i++) {
			tabs[i].classList.remove("selected");
		}
		tabs[idx].classList.add("selected");
		for (var i = 0; i < views.length; i++) {
			views[i].style.display = 'none';
		}
		views[idx].style.display = "block";
	}
	
	function set_view_by_name(tn) {
		web_bus.fire_event(event_name, tn);
		set_view(names[tn]);
	}
	
	function tab_clicked(e) {
		var tn = extract_anchor(e.currentTarget.querySelector('a').href);
		set_view_by_name(tn);
		e.preventDefault();
		return false;
	}
	
	function setup() {
		tabs = el.querySelectorAll(".webbus--tab-box__tab");
		views = el.querySelectorAll(".webbus--tab-box__view");
		for (var i = 0;  i < tabs.length; i++) {
			var n = extract_anchor(tabs[i].querySelector('a').href);
			names[n] = i;
			var shortcut = tabs[i].getAttribute("DATA-SHORTCUT");
			if (shortcut) {
				setup_shortcut(n, shortcut);
			}
		}
		for (var i = 0; i < tabs.length; i++) {
			tabs[i].addEventListener("click", tab_clicked);
		}
		if (params.hasOwnProperty('start_tab')) {
			set_view(params.start_tab);
		}
		else {
			set_view(0);
		}
		
		if (params.hasOwnProperty('event_name')) {
			event_name = params.event_name;
		}
	}
	
	function setup_shortcut(tn, code) {
		codes = code.split('+');
		if (codes.length == 2) {
			if (codes[0] == 'a') {
				// Alt key code
				alt_codes[codes[1]] = tn;
			}
			if (codes[0] == 'c') {
				// Control key code
				ctrl_codes[codes[1]] = tn;
			}
			if (codes[0] == 'ca') {
				// ctrl + alt key combinations
				combo_codes[codes[1]] = tn;
			}
		}
	}
	
	function extract_anchor(url) {
		var parts = url.split('#');
		if (parts.length == 2) {
			return parts[1];
		}
		return false;
	}
}