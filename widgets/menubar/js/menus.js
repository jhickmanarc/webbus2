function webbus__menus_factory(web_bus, name, el, params) {
	this._setup = function(d) {
		setup_ui();
	};
	
	this._start = function(d) {};
	
	this.event_create_menus = function (m) {
		build_menus(m);
	};
	
	var sub_menus = [];
	var event_name = "event_menu_item_activated";
	
	function build_menus(m) {
		sub_menus = [];
		el.innerHTML = '';
		el.appendChild(build_menu(m, 0));
	}
	
	function build_menu(m, depth) {
		var el_menu = document.createElement("UL");
		if(depth > 0) {
			el_menu.style.display = "none";
			sub_menus.push(el_menu);
			el_menu.classList.add("webbus-menu__menu-is-sub-menu");
		}
		else {
			el_menu.classList.add("webbus-menu__menu-is-top-menu");
		}
		el_menu.classList.add("webbus-menu__menu__depth-" + depth);
		for(var i = 0; i < m.length; i++) {
			var el_item = document.createElement("LI");
			el_item.classList.add("webbus-menu__item__depth-" + depth);
			
			if (i == 0) {
				el_item.classList.add("webbus-menu__item__first");
			}
			
			if (i == (m.length - 1)) {
				el_item.classList.add("webbus-menu__item__last");
			}
			
			el_label = document.createElement("SPAN");
			el_label.classList.add("webbus-menu__item-label");
			el_label.innerHTML = m[i].label;
			el_item.appendChild(el_label);
			
			if (m[i].hasOwnProperty("menu")) {
				el_item.classList.add("webbus-menu__menu-has-sub-menu");
				el_item.appendChild(build_menu(m[i].menu, depth + 1));
			}
			if (m[i].hasOwnProperty("action")) {
				el_item.setAttribute("DATA-ACTION", m[i].action);
			}
			el_item.addEventListener("click", item_clicked);
			el_menu.appendChild(el_item);
		}
		return el_menu;
	}
	
	function menu_activation(el_item) {
		if (el_item.parentElement.classList.contains("webbus-menu__menu-is-top-menu")) {
			close_all();
		}
		var state = 0;
		if (el_item.children.length > 1) {
			if (el_item.children[1].style.display == "none") {
				el_item.children[1].style.display = "block";
				el_item.classList.add("webbus-menu__item__is-expanded");
				state = 1;
			}
			else {
				el_item.children[1].style.display = "none";
				el_item.classList.remove("webbus-menu__item__is-expanded");
				state = -1;
			}
		}
		var action = el_item.getAttribute("DATA-ACTION");
		if (action) {
			web_bus.fire_event(event_name, {
				state: state,
				action: action
			});
			if (state == 0) {
				close_all();
			}
		}
	}
	
	function close_all() {
		for (var i = 0; i < sub_menus.length; i++) {
			sub_menus[i].style.display = "none";
			sub_menus[i].parentElement.classList.remove("webbus-menu__item__is-expanded");
		}
	}
	
	function item_clicked(e) {
		var el_item = e.currentTarget;
		menu_activation(el_item);
		e.preventDefault();
		e.stopPropagation();
	}
	
	function background_clicked(e) {
		close_all();
	}
	
	function setup_ui() {
		document.addEventListener("click", background_clicked);
		if (params.hasOwnProperty("event_name")) {
			event_name = params.event_name;
		}
		if (params.hasOwnProperty("menu")) {
			build_menus(params.menu);
		}
	}
}