/*
 * Web Bus JavaScript component manager.
 * Version 2.
 * No external dependencies.
 * Allows run-time creation of DOM containers and dynamic binding from running Components.
 * 
 * (c) 2017 - James Hickman
 * 
 * Licensed under the Lesser GNU GPL (LGPL)
 * 
 * Available prototypes must be registered with the register method passing a name and the factory function.
 * 
 * Component Factories are passed:
 * 		The WebBus object, Instance Name, bound DOM node, custom parameters
 * 
 * Changes:
 * 		Aug 2022 - Added support for the "app" attribute. Support for the <PARAMS> tag as alternate to property.
 */
function _webbus_factory(page_config) {
	/*
	 * Local variables
	 */
	var self = this;
	var components = {};
	var prototypes = {};
	var dom_paths = {};
	var shared_functions = {};
	var apps = {};
	
	this.handler_missing_factory = function (web_bus, cname, logic, dom_element, params) {
		console.log("WebBus ERROR: No Factory registered for '" + logic + "' requested in '" + cname + "'");
	};
	
	function build_path(dom_element) {
		/*
		 * Construct path of parent containers with IDs.
		 */
		var path = [];
		var current = dom_element.parentElement;
		while (current.parentElement !== null) {
			if (current.id !== '') {
				path.unshift(current.id);
			}
			current = current.parentElement;
		}
		return path.join('/');
	}
	
	/*
	 * Public API
	 */
	
	this.fire_event = function (handler_path, data, app_name) {
		/*
		 * Fire an event to the public members of Components.
		 */
		var handler = '', path = '';
		var split_at = handler_path.lastIndexOf('/');
		if (split_at == -1) {
		 	handler = handler_path;
		}
		else {
		 	// Path specified
		 	path = handler_path.substring(0, split_at);
		 	handler = handler_path.substring(split_at + 1);
		}
		var results = {};
		var call_set = null;
		if (app_name !== undefined && app_name !== false && app_name !== null) {
			if (apps.hasOwnProperty(app_name)) {
				call_set = apps[app_name];
			}
			else {
				return false;
			}
		}
		else {
			call_set = components;
		}
		for (m in call_set) {
			if (m != null && typeof call_set[m] === 'object') {
				if (call_set[m][handler] != null && typeof call_set[m][handler] === 'function') {
					if (path == '' || dom_paths[m].indexOf(path) != -1)
						results[m] = call_set[m][handler](data);
				}
			}
		}
		return results;
	};
	
	this.page_config = function (key) {
		return page_config[key];
	};
	
	this.register = function(name, f_factory) {
		prototypes[name] = f_factory;
		return this;
	}
	
	this.construct = function (cname, logic, dom_element, params, app_name) {
		/*
		 * Set up and attach a Component instance to specified DOM element with parameters.
		 */
		if (typeof prototypes[logic] == 'function') {
			var module = new prototypes[logic](self, cname, dom_element, params);
			dom_paths[cname] = build_path(dom_element);
			components[cname] = module;
			if (app_name !== undefined && app_name !== false && app_name !== null) {
				if (!apps.hasOwnProperty(app_name)) {
					apps[app_name] = {};
				}
				apps[app_name][cname] = module;
			}
		}
		else {
			this.handler_missing_factory(self, cname, logic, dom_element, params);
		}
		return this;
	};
	
	this.boot_component = function(cname, logic, dom_element, params) {
		/*
		 * Dynamically set up a Component firing the
		 * standard startup methods.
		 */
		self.construct(cname, logic, dom_element, params);
		components[cname]._setup({});
		components[cname]._start({});
		return this;
	};
	
	this.autostart = function() {
		/*
		 * Scan the DOM for the 'webbus_container' class DIVs and init.
		 * Fire off _setup then _start when done.
		 */
		var containers = document.getElementsByClassName('webbus_container');
		for (var i = 0; i < containers.length; i++) {
			var container = containers[i];
			if (container.nodeName.toLowerCase() == 'div') {
				var id = container.id;
				var logic = container.getAttribute('DATA-LOGIC');
				var params = container.getAttribute('DATA-PARAMS');
				if (!params) {
					// Look for a <PARAMS> tag
					var n_config = container.getElementsByTagName("PARAMS");
					if (n_config.length > 0) {
						params = n_config[0].innerHTML;
					}
				}
				var app_name = container.getAttribute('DATA-APP');
				var param_o = {};
				if (params != null && params.length > 0) {
					try {
						param_o = eval('(' + params + ')');
					}
					catch(err) {
						console.log("WebBus ERROR: Cannot parse params for '" + id + "'");
					}
				}
				self.construct(id, logic, container, param_o, app_name);
			}
		}
		self.fire_event('_setup', {});
		self.fire_event('_start', {});
		return this;
	};
	
	this.export = function (fname, fctn) {
		if (typeof fctn === "function") {
			shared_functions[fname] = fctn;
		}
		return this;
	};
	
	this.import = function (fname, context) {
		if (shared_functions.hasOwnProperty(fname)) {
			return shared_functions[fname].bind(context);
		}
		return false;
	};
	
	this.import_src = function (fname) {
		if (shared_functions.hasOwnProperty(fname)) {
			return shared_functions[fname].toString();
		}
		return false;
	};

	this.rest_call = function(uri, event, data, verb, headers, cb_error, cb_progress) {
		if (verb == undefined) {
			verb = "GET";
		}
		const xhr = new XMLHttpRequest();
		xhr.open(verb, uri, true);
		if (Array.isArray(headers)) {
			for (var i = 0; i < headers.length; i++) {
				xhr.setRequestHeader(headers[i][0], headers[i][1]);
			}
		}
		xhr.onload = function () {
			if (this.readyState == 4) {
				if (this.status == 200) {
					this.fire_event(event, this.responseText);
				}
				else {
					if (typeof cb_error == 'function') {
						cb_error(this.status, this.statusText);
					}
				}
			}
		}
		if (typeof cb_progress == 'function') {
			xhr.upload.addEventListener("progress", cb_progress, false);
		}
		if (data !== undefined) {
			xhr.send(data);
		}
		else {
			xhr.send();
		}
	};
	
	return this;
}
