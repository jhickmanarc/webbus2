/*
 * Simple control key combination manager for Web Bus
 */
 
function _webbus_simple_combo_keys(web_bus) {
	var CTRL_CODE = 17;
	var ALT_CODE = 18;
	
	var ctrl = false;
	var alt = false;
	
	function key_down(e) {
		var keyID = (window.event) ? event.keyCode : e.keyCode;
		if (keyID == CTRL_CODE) {
			ctrl = true;
		}
		else if (keyID == ALT_CODE) {
			alt = true;
		}
		else {
			var key = String.fromCharCode(keyID).toLowerCase();
			if (ctrl && !alt) {
				web_bus.fire_event("event_shortcut_ctrl", key);
			}
			if (alt && !ctrl) {
				web_bus.fire_event("event_shortcut_alt", key);
			}
			if (ctrl && alt) {
				web_bus.fire_event("event_shortcut_ctrlalt", key);
			}
		}
	}
	
	function key_up(e) {
		var keyID = (window.event) ? event.keyCode : e.keyCode;
		if (keyID == CTRL_CODE) {
			ctrl = false;
		}
		if (keyID == ALT_CODE) {
			alt = false;
		}
	}
	
	window.addEventListener("keyup", key_up);
	window.addEventListener("keydown", key_down);
	
	return web_bus;
}