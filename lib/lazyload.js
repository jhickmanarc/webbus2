/*
 * Assign the WebBus factory missing event to
 * a demand loader to load any component defined in
 * the scripts map.
 *
 * Note: This makes the order of component initialization 
 * calls to _setup() and _start() NON-deterministic!
 */
function _webbus_lazy_loader(web_bus, map) {
	web_bus.handler_missing_factory = function (wb, cname, logic, dom_element, params) {
		if (map.hasOwnProperty(logic)) {
			var script = get_script_tag(map[logic].src);
			script[0].addEventListener('load', function () {
				wb.register(logic, window[map[logic].factory]);
				wb.boot_component(cname, logic, dom_element, params);
			});
			if (script[1]) {
				document.head.appendChild(script[0]);
			}
		}
	};
	
	function get_script_tag(src) {
		var clean_src = src.replace("../", '');
		var scps = document.querySelectorAll('script');
		for (var i = 0; i < scps.length; i++) {
			if (scps[i].src.indexOf(clean_src) !== -1) {
				return [scps[i], false]; 
			}
		}
		var sc = document.createElement('script');
		sc.src = src;
		return [sc, true];
	}
	
	return web_bus;
}